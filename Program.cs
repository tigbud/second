﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using HtmlAgilityPack;

namespace ConsoleApp4
{
    class Program
    {
        private static readonly HtmlWeb web = new HtmlWeb {UseCookies = true};

        static void Main(string[] args)
        {
            using StreamWriter file = new StreamWriter("file.txt", true)
            {
                AutoFlush = true
            };
            
            for (var page = 0; page < 1; page++)
            {
                var firstUrl = new Uri($@"https://www.avito.ru/sankt-peterburg/kvartiry/prodam/novostroyka?p={page}");

                var htmlDoc = web.Load(firstUrl);

                var urls = htmlDoc.DocumentNode.SelectNodes("//div/h3/a[@class='item-description-title-link']");

                var list = urls
                    .Select(url => url.GetAttributeValue("href", null))
                    .Select(url => new Uri(firstUrl, url))
                    .ToList();

                var lines = list.AsParallel().SelectMany(ProcessOffer);

                foreach (var line in lines)
                {
                    file.WriteLine(line);
                }
            }
        }

        public class Data
        {
            public string price { get; }
            public int picCount { get; }
            public string address { get; }
            public string title { get; }

            public Data(string price, int picCount, string address, string title)
            {
                this.price = price;
                this.picCount = picCount;
                this.address = address;
                this.title = title;
            }
        }

        private static List<string> ProcessOffer(Uri offerUrl)
        {
            var htmlDocItem = web.Load(offerUrl);

            var itemlist = htmlDocItem.DocumentNode.SelectNodes("//li[@class='item-params-list-item']")
                .Select(itemNode => HttpUtility.HtmlDecode(itemNode.InnerText).Trim())
                .ToList();
            
            var data = GetData(htmlDocItem);

            var file = new List<string>();

            file.Add(data.title);
            file.AddRange(itemlist);
            file.Add("Цена: " + data.price + " рублей");
            file.Add("Количество фотографий в объявлении: " + data.picCount);
            file.Add("Адрес: " + data.address);

            return file;
        }

        private static Data GetData(HtmlDocument htmlDocItem)
        {
            var priceXpath = "//span[@class='js-item-price']";
            var picCountXpath = "//span[@class='gallery-list-item-link']";
            var addresXpath = "//span[@class='item-address__string']";
            var titleXpath = "//h1/span[@class='title-info-title-text']";

            return new Data(
                htmlDocItem.DocumentNode.SelectSingleNode(priceXpath).InnerText,
                htmlDocItem.DocumentNode.SelectNodes(picCountXpath)?.Count ?? 0,
                htmlDocItem.DocumentNode.SelectSingleNode(addresXpath).InnerText.Trim(),
                htmlDocItem.DocumentNode.SelectSingleNode(titleXpath).InnerText);
        }
    }
}